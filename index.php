<?php
    require "vendor/autoload.php";

    $app = new \Slim\App();
    $container = $app->getContainer();

    $container["view"] = function ($c) {
        $view = new \Slim\Views\Twig([
            "templates"
        ]);
        $view->addExtension(new \Slim\Views\TwigExtension(
            $c["router"],
            $c["request"]->getUri()
        ));
        return $view;
    };
    
    $app->get("/", function ($request, $response, $args) {
        return $this->view->render($response, "home.html", [
            "title" => "HARD GLASS",
            "description" => "Hard glass",
            "keywords" => "keywords"
        ]);
    });

    $app->post("/contact", function($request, $response) {
        $body = $request->getParsedBody();
        $to = "midasmdgroup@gmail.com";
        $sendfrom   = "hardglass@yandex.ru";
        $headers = "From: " . strip_tags($sendfrom) . "\r\n" .
                    "Reply-To: " . strip_tags($sendfrom) . "\r\n" .
                    "Content-Type: text/html;charset=utf-8 \r\n" .
                    "X-Mailer: PHP/" . phpversion();
        $subject = "Заявка на сайте Hard Glass";
        $message = $this->view->render($response, "mail.html", $body);
        $send = mail($to, $subject, $message, $headers);
        if ($send) {
            return $response->withStatus(200);
        } else {
            return $response->withStatus(400);
        }
    });

    $container["notFoundHandler"] = function ($c) {
        return function ($request, $response) use ($c) {
            // return $c->view->render($response, "404.html");
            return $response->withStatus(302)->withHeader("Location", "/");
        };
    };

    $app->run();