const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        home: path.resolve('./scripts/index.js')
    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].min.js',
        publicPath: '/dist/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
                
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: [
                    { 
                        loader: 'css-loader', 
                        options: { 
                            importLoaders: 1,
                            sourceMap: true 
                        } 
                    },
                    { 
                        loader: 'postcss-loader', 
                        options: { 
                            sourceMap: true,
                            parser: 'PostCSS',
                            plugins: [
                                require('autoprefixer')({
                                    'browsers': [
                                      'last 2 version'
                                    ],
                                    grid: true
                                  }),
                              ]
                        }
                    },
                    { loader: 'less-loader', options: { sourceMap: true } }
                  ]
                })
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: [
                    { 
                        loader: 'css-loader', 
                        options: { 
                            importLoaders: 1, 
                            sourceMap: true 
                        }
                    }
                  ]
                })
            },
            {
                test: /\.(jpg|png)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        emitFile: false
                    }
                }
            },
            {
                test: /\.svg$/,
                loader: 'svg-inline-loader'
            },
            {
                test: /\.(eot|ttf|woff|woff2|otf)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        publicPath: '/dist/',
                        name: 'fonts/[name].[ext]'
                    }
                }
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin({ filename: '[name].css', allChunks: true }),
    ]
};